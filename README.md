# #3 CI/CD. Домашнее задание 

Наш проект DOGMA-bot, для которого я сдавал ТЗ в качестве первого домашнего задания, пока находится на стадии сбора датасетов, и в нём нет пока кода, который можно было бы использовать в данном задании. Поэтому здесь я разместил код из одного учебного проекта, который я когда-то делал.

## Комментарии к пунктам домашнего задания
_(Для удобства проверки)_

### 1. Шаблонизация. Python пакеты и CLI. 
_смотреть commit_ [Create project from template](https://gitlab.com/lekrott/ods-mlops-03homerwork/-/tree/bdc7ddd94a9f40d1de6af19dcf979b5a232a8807)
- структура проекта была создана на основании нашего собственного шаблона, для него был сделан отдельный [репо](https://gitlab.com/lekrott/cookiecutter-dogma).
- в качестве менеджера пакетов используется `poetry`
- я пока не настроил хранилище для `dvc`, оно будет толкьо к сдаче следующей домашки. Поэтому обучающие данные для моделей пока кладу на GitLab, там один файл .csv небольшого размера.

### 2. Codestyle, инструменты форматирования, линтеры
_смотреть commit_ [Add linters and autoformat, format code accordingly](https://gitlab.com/lekrott/ods-mlops-03homerwork/-/tree/01b9f2db3d5833d1d00d4e1c1458c407dd0df649)
- я установил локально линтеры `flake8` и `pylint`, а также автоформаттер `black`, интегрировал всё это с IDE (у меня `VS Code)
- также настроил локально хуки `pre-commit`, которые отрабатывают `flake8` и `pylint` перед каждым коммитом

### 3. Хранение и версионирование кода
#### A. Имитация code-review и разрешение комментариев
_смотреть merge request_ [Add catboost classifier to models](https://gitlab.com/lekrott/ods-mlops-03homerwork/-/merge_requests/1)
#### Б. Имитация merge-конфликта и его разрешение
_смотреть merge request_ [Set catboost iterations to 1900](https://gitlab.com/lekrott/ods-mlops-03homerwork/-/merge_requests/3)

### 4. Gitlab CI
_смотреть commit_ [Configure CI/CD on GitLab](https://gitlab.com/lekrott/ods-mlops-03homerwork/-/commit/38a092b2501cbbdc1479f71939c1ca1205ff9d56#587d266bb27a4dc3022bbed44dfa19849df3044c)
- я настроил runner на виртуальной машине в Google Cloud Platform _(у меня там сейчас триал-период, на который дают 300 евро. Правда эта vm у меня включена не всегда, чтобы денежка зря не капала)_
- есть стадия **linting**, на ней запускаются `flake8` и `pylint`
- есть стадия **testing**, на ней устанавливаются зависимости и запускаются тесты, которые фитят модели на обучающем датасете, и проверяют, что метрики получаются не ниже заданного уровня