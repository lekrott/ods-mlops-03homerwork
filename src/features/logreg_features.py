""" Converts dataset into features usable to fit the logistic regressiom model on
"""
from sklearn.compose import make_column_transformer
from sklearn.preprocessing import StandardScaler, OneHotEncoder

NUMERIC_COLUMNS = ["ClientPeriod", "MonthlySpending", "TotalSpent"]

CATEGORY_COLUMNS = [
    "Sex",
    "IsSeniorCitizen",
    "HasPartner",
    "HasChild",
    "HasPhoneService",
    "HasMultiplePhoneNumbers",
    "HasInternetService",
    "HasOnlineSecurityService",
    "HasOnlineBackup",
    "HasDeviceProtection",
    "HasTechSupportAccess",
    "HasOnlineTV",
    "HasMovieSubscription",
    "HasContractPhone",
    "IsBillingPaperless",
    "PaymentMethod",
]


def logreg_features(data):
    """Converts input dataset into features to be used to fit
        the logistic regression estimator.
        Does numerical columns scaling and categorical columns one-hot encoding.

    Args:
        data (pd.DataFrame): cleaned dataset

    Returns:
        np.ndarray: numpy array of a ready features
    """
    col_trans = make_column_transformer(
        (StandardScaler(), NUMERIC_COLUMNS),
        (OneHotEncoder(), CATEGORY_COLUMNS)
    )

    features = col_trans.fit_transform(data)

    return features
