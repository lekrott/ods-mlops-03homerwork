""" Script for checking basic metrics for catboost,
    just to make sure everything is working
"""
from src.data import load_raw_data, clean_data, split_train_test
from src.models import rocauc_score, catboost_fit

TARGET_COLUMN = "Churn"


def collect_catboost_scores(
    model, train_features, train_target, test_features, test_target
):
    """Get score metrics both for train and test datasets separately

    Args:
        model: estimator to get scores for
        train_features (pd.DataFrame): train dataset features
        train_target (pd.DataFrame): train dataset lables
        test_features (pd.DataFrame): test dataset features
        test_target (pd.DataFrame): test dataset labels

    Returns:
        Dict: ROC AUC metric scores for train and test datasets respectively
    """
    train_auc = rocauc_score(model, train_features, train_target)
    test_auc = rocauc_score(model, test_features, test_target)
    return {"train_rocauc": train_auc, "test_rocauc": test_auc}


def test_catboost_metrics():
    """pytest test checking if catboost ROC AUC metrics
    bot for train and test dataset are not less than 0.8
    """
    raw_data = load_raw_data()
    cleaned_data = clean_data(raw_data)

    train_data, test_data = split_train_test(cleaned_data)

    catboost_train_features = train_data.drop(TARGET_COLUMN, axis=1)
    catboost_train_target = train_data[TARGET_COLUMN]
    catboost_model = catboost_fit(
        features=catboost_train_features, target=catboost_train_target
    )

    catboost_test_features = test_data.drop(TARGET_COLUMN, axis=1)
    catboost_test_target = test_data[TARGET_COLUMN]
    catboost_results = collect_catboost_scores(
        model=catboost_model,
        train_features=catboost_train_features,
        train_target=catboost_train_target,
        test_features=catboost_test_features,
        test_target=catboost_test_target,
    )
    print("catboost results: ", catboost_results)

    assert catboost_results["train_rocauc"] > 0.8
    assert catboost_results["test_rocauc"] > 0.8
