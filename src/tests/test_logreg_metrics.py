""" Script for checking basic metrics for logistic regression,
    just to make sure everything is working
"""
from src.data import load_raw_data, clean_data, split_train_test
from src.features import logreg_features
from src.models import rocauc_score, logreg_fit

TARGET_COLUMN = "Churn"


def collect_logreg_scores(
    model, train_features, train_target, test_features, test_target
):
    """Get score metrics both for train and test datasets separately

    Args:
        model: estimator to get scores for
        train_features (pd.DataFrame): train dataset features
        train_target (pd.DataFrame): train dataset lables
        test_features (pd.DataFrame): test dataset features
        test_target (pd.DataFrame): test dataset labels

    Returns:
        Dict: ROC AUC metric scores for train and test datasets respectively
    """
    train_auc = rocauc_score(model, train_features, train_target)
    test_auc = rocauc_score(model, test_features, test_target)
    return {"train_rocauc": train_auc, "test_rocauc": test_auc}


def test_logreg_metrics():
    """pytest test checking if logistic regresstino ROC AUC metrics
    bot for train and test dataset are not less than 0.8
    """
    raw_data = load_raw_data()
    cleaned_data = clean_data(raw_data)

    train_data, test_data = split_train_test(cleaned_data)

    logreg_train_features = logreg_features(train_data.drop(TARGET_COLUMN, axis=1))
    logreg_train_target = train_data[TARGET_COLUMN]
    logreg_model = logreg_fit(
        features=logreg_train_features, target=logreg_train_target
    )

    logreg_test_features = logreg_features(test_data)
    logreg_test_target = test_data[TARGET_COLUMN]
    logreg_results = collect_logreg_scores(
        model=logreg_model,
        train_features=logreg_train_features,
        train_target=logreg_train_target,
        test_features=logreg_test_features,
        test_target=logreg_test_target,
    )
    print("logreg results: ", logreg_results)

    assert logreg_results["train_rocauc"] > 0.8
    assert logreg_results["test_rocauc"] > 0.8
