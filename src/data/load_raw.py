""" Utility for loading original raw data to be used overall project
"""

import pandas as pd


def load_raw_data():
    """Loads raw Churn dataset from CSV file

    Returns:
        pd.DataFrame: dataframe with raw Churn data
    """
    raw_data = pd.read_csv("data/raw/churn.csv")

    return raw_data
