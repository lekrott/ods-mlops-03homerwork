"""Utility funtions for cleaning raw data, like type casting, fill NaNs
"""
import pandas as pd

NUMERIC_COLUMNS = ["ClientPeriod", "MonthlySpending", "TotalSpent"]


def clean_data(raw_data: pd.DataFrame) -> pd.DataFrame:
    """General data cleaning

    Args:
        raw_data (pd.DataFrame): a Churn dataframe loaded from original CSV file

    Returns:
        pd.DataFrame: copy of raw dataframe with a cleaned data
    """
    result = raw_data.copy()
    for col in NUMERIC_COLUMNS:
        result[col] = pd.to_numeric(result[col], errors="coerce")
    result[NUMERIC_COLUMNS] = result[NUMERIC_COLUMNS].fillna(0)

    return result
